i = open("purchases.txt", "r")
o = open("o.txt", "w")

#individual variables for floats and strings had to be made as you can't
#typeCast within a write statement.

sum = 0.0
currCostFloat = 0.0
sumString = ""

topCost = 0.0
topCostString = ""

botCost = 9999.99
botCostString = ""

averageCount = 0
average = 0.0
averageString = ""

dataCostList = []
dataCostListString = ""


#reading/writing loop written by Dr.Case
#also finds high/low/average/count written by group

for line in i:
  data = line.strip().split('    ')
  if (len(data) == 6):
    date, time, store, item, cost, payment = data
    print(data)
    currCostFloat = float(cost)
    dataCostList.append(currCostFloat)
    if (currCostFloat>topCost):
      topCost = currCostFloat
    if (currCostFloat<botCost):
      botCost = currCostFloat
    if (payment=="Visa"):
      sum += currCostFloat
    #if (payment == "MasterCard"):
    # sum = sum + currCostFloat  
    averageCount+=1
    o.write(store + "\t" + cost + "\n")

#writing statements. Convert calculated floats into strings before writing
#due to inability to typecast within a write statement.
    
o.write("\n")    
sumString = str(sum)
o.write("The sum of all Visa cards are: $"+sumString +"\n")

topCostString = str(topCost)
o.write("The highest cost is: $"+topCostString+"\n")

botCostString = str(botCost)
o.write("The lowest cost is: $"+botCostString+"\n")

average = (sum/averageCount)
averageString = str("%.2f" % average)
o.write("The average cost is: $"+ averageString+"\n")

o.write("The top five costs are: \n")
dataCostList.sort()
for x in range(0,5):
  dataCostListString=str(dataCostList.pop())
  o.write("$"+dataCostListString + "\n")

i.close()
o.close()
